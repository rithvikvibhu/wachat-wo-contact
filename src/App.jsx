import { useEffect, useState } from 'react';
import c from 'classnames';

import 'react-phone-number-input/style.css';
import PhoneInput from 'react-phone-number-input';
import { isPossiblePhoneNumber } from 'react-phone-number-input';

function openWhatsapp(number) {
  if (!number) return;
  if (number[0] === '+') number = number.slice(1);
  // window.location = 'https://wa.me/' + number;
  window.location = `whatsapp://send/?phone=${number}&text&type=phone_number&app_absent=0`;
}

function App() {
  const [number, setNumber] = useState();
  const isValid = number && isPossiblePhoneNumber(number);

  const [country, setCountry] = useState();
  useEffect(() => {
    (async () => {
      const res = await fetch('https://ipapi.co/json/');
      const data = await res.json();
      if (data.country_code) setCountry(data.country_code);
    })();
  }, []);

  return (
    <div className='text-white text-center'>
      <h1 className='mt-8 font-bold text-2xl'>WAChat w/o Contact</h1>

      <p className='mt-8 container px-12 mx-auto font-medium'>
        Start a chat on WhatsApp without saving the number as a contact
      </p>

      <div className='mt-8 flex flex-wrap gap-4 items-center justify-center text-gray-900'>
        <PhoneInput
          defaultCountry={country}
          placeholder='Enter phone number'
          value={number}
          onChange={setNumber}
          style={{
            '--PhoneInputCountryFlag-height': '2rem',
          }}
        />
        <div className='w-full sm:w-auto justify-center'>
          <button
            className={c(
              'px-3 py-1 sm:py-2 rounded',
              { 'shadow bg-green-500': isValid },
              { 'shadow-none bg-gray-300 cursor-not-allowed': !isValid }
            )}
            disabled={!isValid}
            onClick={() => openWhatsapp(number)}
          >
            START CHAT
          </button>
        </div>
      </div>

      <div className='fixed bottom-0 w-full md:px-1/4 bg-green-700'>
        <div className='px-4 sm:px-16 h-8 py-2 flex items-baseline justify-around md:justify-center md:space-x-8 text-sm font-medium text-green-100'>
          <a
            target='_blank'
            href='https://gitlab.com/rithvikvibhu/wachat-wo-contact'
          >
            GitLab
          </a>
          <span>&nbsp;</span>
          <a target='_blank' href='https://blek.ga'>
            Rithvik Vibhu
          </a>
        </div>
      </div>
    </div>
  );
}

export default App;
